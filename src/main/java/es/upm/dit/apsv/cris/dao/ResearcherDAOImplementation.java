package es.upm.dit.apsv.cris.dao;

import java.util.List;

import org.hibernate.Session;

import es.upm.dit.apsv.cris.model.Researcher;

public class ResearcherDAOImplementation implements ResearcherDAO {
	private static ResearcherDAOImplementation instance = null;
	private ResearcherDAOImplementation() {	}
	public static ResearcherDAOImplementation getInstance() {
		if (null == instance)
			instance = new ResearcherDAOImplementation();
		return instance;
	}
	
	@Override
	public Researcher create(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
		  session.beginTransaction();
		  // persistence operations
		  session.save(researcher);
		  session.getTransaction().commit();
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close();
		}
		return researcher;
	}

	@Override
	public Researcher read(String researcherId) {
		Session session = SessionFactoryService.get().openSession();
		Researcher r = null;
		try {
		  session.beginTransaction();
		  // persistence operations
		  r = session.get(Researcher.class, researcherId);
		  session.getTransaction().commit();
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close();
		}
		return r;
	}

	@Override
	public Researcher update(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
		  session.beginTransaction();
		  session.saveOrUpdate(researcher);
		  session.getTransaction().commit();
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close();
		}
		return researcher;
	}

	@Override
	public Researcher delete(Researcher researcher) {
		Session session = SessionFactoryService.get().openSession();
		try {
		  session.beginTransaction();
		  session.delete(researcher);
		  session.getTransaction().commit();
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close();
		}
		return researcher;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Researcher> readAll() {
		Session session = SessionFactoryService.get().openSession();
		List<Researcher> l = null;
		try {
		  session.beginTransaction();
		  l = (List<Researcher>) session.createQuery("from Researcher").getResultList();
		  session.getTransaction().commit();
		} catch (Exception e) {
		  // handle exceptions
		} finally {
		  session.close();
		}
		return l;
	}

	@Override
	public Researcher readByEmail(String email) {
		for (Researcher r : this.readAll())
			if (email.equals(r.getEmail()))
				return r;
		return null;
	}
	
	@Override
	public Researcher readAsUser(String email, String password) {
		Researcher researcher = null;
		Session session = SessionFactoryService.get().openSession();
		try {
			   session.beginTransaction();
			   researcher = (Researcher) session.createQuery("select r from Researcher r where r.email= :email and r.password = :password")
				        .setParameter("email", email)
				        .setParameter("password", password)
				        .uniqueResult();
			   session.getTransaction().commit();
			} catch (Exception e) {
			   System.out.print(e);
			} finally {
			   session.close();
			}

		return researcher;
	}


}
